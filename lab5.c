#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

int *vector;
int readers, writers;
int nthreads;

pthread_mutex_t reader_mutex;
pthread_mutex_t writer_mutex;
pthread_cond_t reader_cond;
pthread_cond_t writer_cond;

void writer_barrier(int i) {
    pthread_mutex_lock(&writer_mutex);
    writers--;
    if (writers > 0) {
        pthread_cond_wait(&writer_cond, &writer_mutex);
    } else {
        writers=nthreads-(2*i);
        pthread_cond_broadcast(&writer_cond);
    }
    pthread_mutex_unlock(&writer_mutex);
}

void reader_barrier(int i)
{
    pthread_mutex_lock(&reader_mutex);
    readers--;
    if (readers > 0) {
        pthread_cond_wait(&reader_cond, &reader_mutex);
    } else {
        readers=nthreads-(2*i);
        pthread_cond_broadcast(&reader_cond);
    }
    pthread_mutex_unlock(&reader_mutex);
}

void *task(void *threadId)
{
    int id = *(int *) threadId;
    int aux;
    for(int i=1; i<=id; i*=2)
    {
        aux = vector[id-i];
        reader_barrier(i);
        vector[id] += aux;
        writer_barrier(i);
    }
    
    pthread_exit(NULL);
}

int prefixSum(int size)
{
    pthread_t tid[size];
    int threads[size];

    pthread_mutex_init(&reader_mutex, NULL);
    pthread_mutex_init(&writer_mutex, NULL);
    pthread_cond_init(&reader_cond, NULL);
    pthread_cond_init(&writer_cond, NULL);

    for(int i=0; i<size; i++){
        threads[i]=i;
        if(pthread_create(&tid[i], NULL, task, (void *)(threads + i))){
         fprintf(stderr, "ERRO--pthread_create\n");
         return 3;
      }
    }

    for (int i = 0; i<size; i++)
    {
        pthread_join(tid[i], NULL);
    }

    pthread_mutex_destroy(&reader_mutex);
    pthread_mutex_destroy(&writer_mutex);
    pthread_cond_destroy(&reader_cond);
    pthread_cond_destroy(&writer_cond);
}

void sampleTest(int size, int *expected)
{
    int error = 0;

    nthreads = size;

    vector = malloc(sizeof(int)*size);

    for(int i=0; i<size; i++) {
        vector[i] = i+1;
    }
    
    prefixSum(size);

    for (int i = 0; i<size; i++)
    {
        if(vector[i] != expected[i]) error++;
    }

    if(!error){
        printf(ANSI_COLOR_GREEN "\nO TESTE PASSOU!\n" ANSI_COLOR_RESET);
    } else {
        printf(ANSI_COLOR_RED "\nO TESTE FALHOU!\n" ANSI_COLOR_RESET);
    }
    
    printf("Valores: { ");
    for (int i = 0; i<size; i++)
    {
        printf(ANSI_COLOR_BLUE "%i " ANSI_COLOR_RESET, vector[i]);
    }
    printf("}\n");

    free(vector);
}

int main(int argc, char *argv[])
{
    int expected1[4] = {1, 3, 6, 10};
    int expected2[8] = {1, 3, 6, 10, 15, 21, 28, 36};
    int expected3[16] = {1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136}; 
    int expected4[32] = {1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136, 
    153, 171, 190, 210, 231, 253, 276, 300, 325, 351, 378, 406, 435, 465, 496, 528};

    readers = 3;
    writers = 3;
    sampleTest(4, expected1);

    readers = 7;
    writers = 7;
    sampleTest(8, expected2);

    readers = 15;
    writers = 15;
    sampleTest(16, expected3);

    readers = 31;
    writers = 31;
    sampleTest(32, expected4);

    printf("\nFIM\n");
    return 0;
}