#!/bin/sh

LOCAL_BIN=./bin
PACKAGE_MONITOR=./bin/monitor

echo Building classes
cd ./src
javac Main.java
cd ../

if [ ! -d "$LOCAL_BIN" ]; then
    mkdir "$LOCAL_BIN"
fi

mv ./src/*.class "$LOCAL_BIN"

if [ ! -d "$PACKAGE_MONITOR" ]; then
    mkdir "$PACKAGE_MONITOR"
fi

mv ./src/monitor/*.class "$PACKAGE_MONITOR"

echo Classes criadas em "$LOCAL_BIN"

echo Installing
cd "$LOCAL_BIN"
jar cfe temperature_system.jar Main Main.class monitor/*.class
cd ../
mv "$LOCAL_BIN"/temperature_system.jar ./temperature_system.jar 

# ln -s "$LOCAL_BIN"/temperature_system.jar temperature_system.bin
echo Testando...
echo ''
java -jar temperature_system.jar
echo ''

echo Concluido!!!
echo "by: Daniel Silva <https://gitlab.com/returndaniels/>"