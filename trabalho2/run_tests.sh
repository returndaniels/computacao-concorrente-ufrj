#!/bin/sh

WITH_LOCAL_LIBS="$(pwd)/lib/junit-4.13.1.jar:$(pwd)/lib/hamcrest-core-1.3.jar:$(pwd)/src/monitor.jar;"

echo Building classes de teste

cd ./src/

# javac *.java

javac -cp "$WITH_LOCAL_LIBS" WriterTest.java
javac -cp "$WITH_LOCAL_LIBS" ReaderTest.java
javac -cp "$WITH_LOCAL_LIBS" WritePriorityMonitorTest.java
javac -cp "$WITH_LOCAL_LIBS" TestRunner.java

# echo Rodando testes
java TestRunner