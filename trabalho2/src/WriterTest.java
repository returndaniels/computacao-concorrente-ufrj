

import java.util.ArrayList;

import monitor.Handler;
import monitor.Monitor;

public class WriterTest extends Handler {
    private ArrayList<String> recorded;

    public WriterTest(int id, int delay, Monitor monitor, ArrayList<String> recorded) {
        super(id, delay, monitor);

        this.recorded = recorded;
    }

    @Override
    public void run() {
        try {
            monitor.startWriting(id);
            Thread.sleep(delay);
            recorded.add("writer");
            monitor.finishWriting(id);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Exception has been caught" + e);
        }
    }
}
