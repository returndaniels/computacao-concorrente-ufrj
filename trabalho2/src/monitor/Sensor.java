package monitor;
import java.util.Random;

public class Sensor extends Handler {
    private Random rand;
    private TemperatureList temperatures;
    private int countTemperature;

    public Sensor(int id, int delay, Monitor monitor, TemperatureList temperatures) {
        super(id, delay, monitor);

        this.temperatures = temperatures;
        rand = new Random();
    }

    @Override
    public void run() {
        Temperature temperature; 
        int temperatureValue;
        int registredValues = 0;
        
        for(;;) {
            try {
                monitor.startWriting(id);
                Thread.sleep(delay);
                temperatureValue = rand.nextInt((40 - 25) + 1) + 25;
                countTemperature = ((WritePriorityMonitor) monitor).incCountSensorReadings();

                if(temperatureValue > 30) {
                    temperature = new Temperature(temperatureValue, id, countTemperature);
                    temperatures.set(registredValues%temperatures.size(), temperature);
                    registredValues++;
                }

                monitor.finishWriting(id);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Exception has been caught" + e);
            }
        }
    }
}
