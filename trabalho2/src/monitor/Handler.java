package monitor;
public abstract class Handler extends Thread {
    protected int id;
    protected int delay;
    protected Monitor monitor;
  
    public Handler(int id, int delay, Monitor monitor) {
        this.id = id;
        this.delay = delay;
        this.monitor = monitor;
    }
  
    public abstract void run();
}
