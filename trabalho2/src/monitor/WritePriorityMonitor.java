package monitor;
public class WritePriorityMonitor extends Monitor {
    private int waitingWriters;
    private int countSensorReadings;

    public WritePriorityMonitor() {
        super();
        this.waitingWriters = 0;
        this.countSensorReadings = 0;
    }

    public synchronized int incCountSensorReadings() {
        return this.countSensorReadings++;
    }


    @Override
    public synchronized void startReading(int id) {
        try { 
            if (super.writers > 0 || this.waitingWriters > 0) {
               wait();
            }
            super.readers++;
        } catch (InterruptedException e) { 
            System.out.println("Exception has been caught" + e);
        }
    }

    @Override
    public synchronized void startWriting(int id) {
        try { 
            if ((super.readers > 0) || (super.writers > 0)) {
                this.waitingWriters++;
                wait();
            }
            this.waitingWriters--;
            super.writers++; 
        } catch (InterruptedException e) { 
            System.out.println("Exception has been caught" + e);
        }
    }
}
