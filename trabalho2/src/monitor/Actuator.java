package monitor;

public class Actuator extends Handler {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_GREEN = "\u001B[32m";

    private TemperatureList temperatures;

    public Actuator(int id, int delay, Monitor monitor, TemperatureList temperatures) {
        super(id, delay, monitor);
        this.temperatures = temperatures;
    }

    private void shiftLeft(Temperature[] ts){
        for( int index = 1; index < ts.length; index++)
            ts[index-1] = ts[index];
    }

    private void printAlert(Temperature[] ts) {
        int over35 = 0;
        for (int i = ts.length-1, c = 1; i >= 0; i--, c++) {
            if(ts[i] != null && ts[i].getValue() > 35) {
                over35++;
            }
            if(c == 5 && over35 == 5) {
                System.out.println(ANSI_RED+id+": ALERTA VERMELHO"+ANSI_RESET);
                return;
            } else if(c == 15 && over35 == 5) {
                System.out.println(ANSI_YELLOW+id+": ALERTA AMARELO"+ANSI_RESET);
                return;
            }
        }
        System.out.println(ANSI_GREEN+id+": CONDIÇÃO NORMAL"+ANSI_RESET);
    }

    @Override
    public void run() {
        Temperature[] recordedTemperatures;

        for(;;) {
            recordedTemperatures = new Temperature[15];
            try {
                monitor.startReading(id);
                Thread.sleep(delay);
                for (int i = 0, currentPosition = 0; i < temperatures.size(); i++) {
                    if(temperatures.get(i) == null) continue;
                    if(temperatures.get(i).getSensorId() == id) {
                        if(currentPosition < 15) {
                            recordedTemperatures[currentPosition] = temperatures.get(i);
                            currentPosition++;
                        } else if(recordedTemperatures[14].getValue() < temperatures.get(i).getValue()) {
                            shiftLeft(recordedTemperatures);
                            recordedTemperatures[14] = temperatures.get(i);
                        }
                    }
                }
                printAlert(recordedTemperatures);
                monitor.finishReading(id);
            } catch(InterruptedException e) {
                e.printStackTrace();
                System.out.println("Exception has been caught" + e);
            }
        }
    }
}
