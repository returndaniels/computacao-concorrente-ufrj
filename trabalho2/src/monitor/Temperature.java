package monitor;

public class Temperature {
    private int value;
    private int sensorId;
    private int sensorReadId;

    Temperature(int value, int sensorId, int sensorReadId) {
        this.value = value;
        this.sensorId = sensorId;
        this.sensorReadId = sensorReadId;
    }

    public int getValue() {
        return value;
    }

    public int getSensorId() {
        return sensorId;
    }

    public int getSensorReadId() {
        return sensorReadId;
    }

    @Override
    public String toString() {
        return Integer.valueOf(value).toString();
    }
}
