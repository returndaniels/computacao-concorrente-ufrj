package monitor;

public class Monitor {
    public int writers;
    public int readers;

    Monitor() {
        this.writers = 0;
        this.readers = 0;
    }

    public synchronized void startReading(int id) {
        try { 
            if (this.writers > 0) {
               wait();
            }
            this.readers++;
        } catch (InterruptedException e) { 
            System.out.println("Exception has been caught" + e);
        }
    }

    public synchronized void finishReading(int id) {
        this.readers--;
        if (this.readers == 0) {
            this.notify();
        }
    }

    public synchronized void startWriting(int id) {
        try { 
            if ((this.readers > 0) || (this.writers > 0)) {
               wait();
            }
            this.writers++; 
        } catch (InterruptedException e) {
            System.out.println("Exception has been caught" + e);
        }
    }

    public synchronized void finishWriting(int id) {
        this.writers--;
        notifyAll();
    }

}