

import java.util.ArrayList;

import monitor.Handler;
import monitor.Monitor;

public class ReaderTest extends Handler {
    private ArrayList<String> recorded;

    public ReaderTest(int id, int delay, Monitor monitor, ArrayList<String> recorded) {
        super(id, delay, monitor);

        this.recorded = recorded;
    }

    @Override
    public void run() {
        try {
            monitor.startReading(id);
            Thread.sleep(delay);
            recorded.add("reader");
            monitor.finishReading(id);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Exception has been caught" + e);
        }
    }
}
