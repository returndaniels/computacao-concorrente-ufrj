
import org.junit.Before;
import org.junit.Test;

import monitor.Handler;
import monitor.WritePriorityMonitor;

import org.junit.Assert;

import java.util.ArrayList;

public class WritePriorityMonitorTest {
    ArrayList<String> recorded;
    WritePriorityMonitor monitor;
    Handler writer1;
    Handler writer2;
    Handler reader1;
    Handler reader2;

    @Before
    public void setUp() {
        recorded = new ArrayList<String>();
        monitor = new WritePriorityMonitor();
        writer1 = new WriterTest(0, 500, monitor, recorded);
        writer2 = new WriterTest(0, 0, monitor, recorded);
        reader1 = new ReaderTest(0, 100, monitor, recorded);
        reader2 = new ReaderTest(1, 50, monitor, recorded);
    }

    @Test
    public void priorityWritingReadingTest() {
        // Inicia threas
        reader1.start();
        writer1.start();

        // Esse sleep serve para agarantir que writer irar inicializar antes
        // da thread seguinte
        try { Thread.sleep(50); } catch(Exception e) {}
        
        reader2.start();
        writer2.start();

        // Aguarda threads terminarem
        try {
            writer1.join();
            writer2.join();
            reader1.join();
            reader2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Exception has been caught" + e);
        }

        // ArrayList: recorded deveria ter os valores [reader, writer, writer, reader]
        // reader2 só pode ser desbloqueado apos os dois writers terminarem
        // System.out.println(recorded);
        Assert.assertTrue(recorded.get(0).equals("reader")); 
        Assert.assertTrue(recorded.get(1).equals("writer")); 
        Assert.assertTrue(recorded.get(2).equals("writer")); 
        Assert.assertTrue(recorded.get(3).equals("reader")); 
    }

}
