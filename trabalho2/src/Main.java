import monitor.*;

public class Main {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";

    public static void main(String[] args) {
        Handler[] reader;
        Handler[] writer;
        int nSensores = 0;

        Monitor monitor = new WritePriorityMonitor();
        TemperatureList temperatures = new TemperatureList(60);

        if(args.length < 1){
            System.out.println("Execute com:");
            System.out.println("\tjava -jar temperature_system.jar <numero de sensores>");
            System.out.println("ou");
            System.out.println("\tcd ./bin");
            System.out.println("\tjava Main <numero de sensores>");
            return;
        }

        nSensores = Integer.parseInt(args[0]);
        reader = new Handler[nSensores];
        writer = new Handler[nSensores];

        System.out.println(ANSI_BLUE+"######################################");
        System.out.println("###### SISTAMA DE MONITORAMENTO ######");
        System.out.println("######      DE TEMPERATURA.     ######");
        System.out.println("######################################\n"+ANSI_RESET);

        System.out.println("---- UTILIZE CTRL+C PARA ENCERRAR O PROGRAMA ----\n");

        for (int i = 0; i < 60; i++) {
            temperatures.add(null);
        }
        
        for (int i = 0; i < nSensores; i++) {
           writer[i] = new Sensor(i+1, 1000, monitor, temperatures);
           writer[i].start(); 
        }
        for (int i=0; i<nSensores; i++) {
           reader[i] = new Actuator(i+1, 2000, monitor, temperatures);
           reader[i].start(); 
        }

        for (int i = 0; i < nSensores; i++) {
            try {
                writer[i].join(); 
            } catch(Exception e){
                System.out.println("Exception has been caught" + e);
            }
        }

        for (int i = 0; i < nSensores; i++) {
            try {
                reader[i].join(); 
            } catch(Exception e){
                System.out.println("Exception has been caught" + e);
            }
         }


        System.out.println(temperatures);
        System.out.println("FIM");
    }
}
