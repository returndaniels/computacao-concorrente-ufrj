import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   static ReaderTest rt;
   static WriterTest wt;
   static WritePriorityMonitorTest tp;

   public static void main(String[] args) {
      tp = new WritePriorityMonitorTest();
      rt = new ReaderTest(0, 0, null, null);
      wt = new WriterTest(0, 0, null, null);

      Result result = JUnitCore.runClasses(WritePriorityMonitorTest.class);
		
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
		
      if(result.wasSuccessful())
         System.out.println("O TESTE PASSOU!!!");
   }
}  	