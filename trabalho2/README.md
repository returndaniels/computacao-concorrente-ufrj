# SISTEMA DE MONITORAMENTO DE TEMPERATURA

Este é o segundo trabalho de Computação Concorrente (MAB117), disciplina do departamento de Ciência da Computação da Universidade Federal do Rio de Janeiro (DCC-UFRJ) [veja](https://dcc.ufrj.br/).


## Objetivo

O objetivo deste programa é criar uma simulação de monitoramento de temperatura de um ambiente com sensores de temperatura e alarmes de incêndio que trabalham concorrentemente.


## Como rodar
OBS: Foi requisto que o programa rodasse por tempo indeterminado, então é necessário digitar ```ctrl+c``` para encerrar a execuçaõ do programa
### Recomendado
Execute o comando na raiz do projeto
```sh
sh install.sh
```

e rode com
```sh
java -jar temperature_system.jar <número de sensores>
```

O número de alarmes será igual ao número de sensores

### Opção (Caso o comando anterio não funcione)
Execute o comando na raiz do projeto
```sh
javac src/*.java
```

Crie o diretório ```bin/```
```sh
mkdir bin
mkdir bin/monitor
```

Mova nos arquivos para o diretório ```bin/```
```sh
mv src/*.class ./bin
mv src/monitor/*.class ./bin/monitor
```

E rode com
```sh
cd ./bin
java Main <número de sensores>
```

O número de alarmes será igual ao número de sensores


### Criar ```.jar``` manualmente
Voce pode criar um arquivo ```.jar``` com
```sh
jar cfe temperature_system.jar Main Main.java monitor/*.class
```

e rodar com 
```sh
java -jar temperature_system.jar
```
