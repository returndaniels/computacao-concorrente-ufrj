#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#define NTHREADS 4

sem_t condt1, condt2, condt3;

void *T4(void *t) {
    printf("T4: Seja bem-vindo!\n");

    sem_post(&condt1); //permite que T3 ou T2 execute

    pthread_exit(NULL);
}

void *T3(void *t) {
    sem_wait(&condt1); //espera T4 executar

    printf("T3: Sente-se por favor.\n");

    sem_post(&condt1); //permite que T2 execute, caso esteja parada
    sem_post(&condt2); //incrementa 1 na condição para que T1 execute

    pthread_exit(NULL);
}

void *T2(void *t) {
    sem_wait(&condt1); //espera T4 executar

    printf("T2: Fique a vontade.\n");

    sem_post(&condt1); //permite que T3 execute, caso esteja parada
    sem_post(&condt3); //incrementa 1 na condição para que T1 execute

    pthread_exit(NULL);
}

void *T1(void *t) {
    sem_wait(&condt2); //espera T3 executar
    sem_wait(&condt3); //espera T2 executar

    printf("T1: Volte sempre!.\n");
    
    pthread_exit(NULL);
}

/* Funcao principal */
int main(int argc, char *argv[])
{
    int i;
    pthread_t threads[NTHREADS];

    //inicia os semaforos
    sem_init(&condt1, 0, 0);
    sem_init(&condt2, 0, 0);
    sem_init(&condt3, 0, 0);

    /* Cria as threads */
    pthread_create(&threads[0], NULL, T1, NULL);
    pthread_create(&threads[1], NULL, T2, NULL);
    pthread_create(&threads[2], NULL, T3, NULL);
    pthread_create(&threads[3], NULL, T4, NULL);

    /* Espera todas as threads completarem */
    for (i = 0; i < NTHREADS; i++)
    {
        if (pthread_join(threads[i], NULL)) {
         printf("--ERRO: pthread_join() \n"); exit(-1); 
    } 
    }
    printf("\nFIM\n");

    return 0;
}
