#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NTHREADS 4

int x = 0;
pthread_mutex_t x_mutex;
pthread_cond_t x_cond;

void *T4(void *t)
{
    printf(ANSI_COLOR_BLUE "T4: Comecei\n" ANSI_COLOR_RESET);

    printf(ANSI_COLOR_GREEN "T4: Seja bem-vindo!\n" ANSI_COLOR_RESET);

    pthread_mutex_lock(&x_mutex);
    x++;
    printf(ANSI_COLOR_GREEN "T4: x = %d, vai sinalizar a condicao \n" ANSI_COLOR_RESET, x);
    pthread_cond_broadcast(&x_cond);
    pthread_mutex_unlock(&x_mutex);

    pthread_exit(NULL);
}

void *T3(void *t)
{
    printf(ANSI_COLOR_BLUE "T3: Comecei\n" ANSI_COLOR_RESET);

    pthread_mutex_lock(&x_mutex);
    if (x == 0)
    {
        printf(ANSI_COLOR_RED "T3: vai se bloquear...\n" ANSI_COLOR_RESET);
        pthread_cond_wait(&x_cond, &x_mutex);
        printf(ANSI_COLOR_GREEN "T3: sinal recebido e mutex realocado, x = %d\n" ANSI_COLOR_RESET, x);
    }
    x++;

    printf(ANSI_COLOR_GREEN "T3: Sente-se por favor.\n" ANSI_COLOR_RESET);
    pthread_mutex_unlock(&x_mutex);
    pthread_cond_broadcast(&x_cond);
    pthread_exit(NULL);
}

void *T2(void *t)
{
    printf(ANSI_COLOR_BLUE "T2: Comecei\n" ANSI_COLOR_RESET);

    pthread_mutex_lock(&x_mutex);
    if (x == 0)
    {
        printf(ANSI_COLOR_RED "T2: vai se bloquear...\n" ANSI_COLOR_RESET);
        pthread_cond_wait(&x_cond, &x_mutex);
        printf(ANSI_COLOR_GREEN "T2: sinal recebido e mutex realocado, x = %d\n" ANSI_COLOR_RESET, x);
    }
    x++;

    printf(ANSI_COLOR_GREEN "T2: Fique a vontade.\n" ANSI_COLOR_RESET);
    pthread_mutex_unlock(&x_mutex);
    pthread_cond_broadcast(&x_cond);
    pthread_exit(NULL);
}

void *T1(void *t)
{
    printf(ANSI_COLOR_GREEN "T1: Comecei\n" ANSI_COLOR_RESET);

    pthread_mutex_lock(&x_mutex);
    while (x < 3)
    {
        printf(ANSI_COLOR_RED "T1: vai se bloquear... x = %d\n" ANSI_COLOR_RESET, x);
        pthread_cond_wait(&x_cond, &x_mutex);
        printf(ANSI_COLOR_GREEN "T1: sinal recebido e mutex realocado, x = %d\n" ANSI_COLOR_RESET, x);
    }
    printf(ANSI_COLOR_GREEN "T1: Volte sempre!.\n" ANSI_COLOR_RESET);
    pthread_mutex_unlock(&x_mutex);
    pthread_exit(NULL);
}

/* Funcao principal */
int main(int argc, char *argv[])
{
    int i;
    pthread_t threads[NTHREADS];

    /* Inicilaiza o mutex (lock de exclusao mutua) e a variavel de condicao */
    pthread_mutex_init(&x_mutex, NULL);
    pthread_mutex_init(&x_mutex, NULL);
    pthread_cond_init(&x_cond, NULL);
    pthread_cond_init(&x_cond, NULL);

    /* Cria as threads */
    pthread_create(&threads[0], NULL, T1, NULL);
    pthread_create(&threads[1], NULL, T2, NULL);
    pthread_create(&threads[2], NULL, T3, NULL);
    pthread_create(&threads[3], NULL, T4, NULL);

    /* Espera todas as threads completarem */
    for (i = 0; i < NTHREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }
    printf("\nFIM\n");

    /* Desaloca variaveis e termina */
    pthread_mutex_destroy(&x_mutex);
    pthread_mutex_destroy(&x_mutex);
    pthread_cond_destroy(&x_cond);
    pthread_cond_destroy(&x_cond);
    return 0;
}
