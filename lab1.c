/**
/* Este programa concorrente, possui duas threads (alem da thread principal), 
/* para incrementar de 1 cada elemento de um vetor de 10000 elementos.
**/
#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>

#define NTHREADS  2
#define NELEMENTS 10000

int *vector;

// Recebe a posição inicial do loop de acordo com a id da thread
void *iterator (void *arg) {
  for(int i=*(int *)arg; i<NELEMENTS; i+=2) {
    vector[i]++;
  }
  pthread_exit(NULL);
}

int main(void) {
  pthread_t tid_sistema[NTHREADS];
  int threads[NTHREADS];

	vector = (int *)calloc(NELEMENTS, sizeof(int));

  for(int th=0; th<NTHREADS; th++) {
    printf("--Cria a thread %d\n", th);
    threads[th]=th;
    if (pthread_create(&tid_sistema[th], NULL, iterator, &threads[th])) {
      printf("--ERRO: pthread_create()\n");
			exit(-1);
    }
  }

	for(int th=0; th<NTHREADS; th++) {
    printf("--Agurada termino das tarefas - thread %d\n", th);
    if (pthread_join(tid_sistema[th], NULL)) {
      printf("--ERRO: pthread_join()\n"); 
			exit(-1);
    }
  }

  printf("--valores do vetor\n[");
	for(int vectorPosition=0; vectorPosition<NELEMENTS; vectorPosition++) {
    printf("%d", vector[vectorPosition]);
  }
	printf("]\n");

  printf("--Thread principal terminou\n");

  pthread_exit(NULL);
}
