class Task extends Thread {
    int[] array;
    int id;
    int nThreads;

    public Task(int[] array, int id, int nThreads) {
        this.array = array;
        this.id = id;
        this.nThreads = nThreads;
    }

    public void run(){
        for (int i = id; i < array.length; i += nThreads) {
            array[i]++;
        }
    }
}

public class Lab6 {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";

    private static void handle(Thread[] threads, int[] array, int nThreads) {
        for (int i=0; i<threads.length; i++) {
            threads[i] = new Task(array, i, nThreads);
        }

        for (int i=0; i<threads.length; i++) {
            threads[i].start();
        }

        for (int i=0; i<threads.length; i++) {
            try { 
                threads[i].join(); 
            } catch (InterruptedException e) {
                return; 
            }
        }
    }

    private static Boolean testAssert(int[] A, int nThreads) {
        int[] array = new int[A.length];
        Thread[] threads = new Thread[nThreads];

        handle(threads, array, nThreads);

        for (int i = 0; i < array.length; i++) {
            if(array[i] != A[i]) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int N, nThreads;
        Thread[] threads;
        int[] array;

        if(args.length == 1 && args[0].equals("--test")) {
            int[] compareArray = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            
            for (int i = 1; i <= compareArray.length+1; i++) {
                System.out.print("Número de threads: "+i);
                if (testAssert(compareArray, i)) {
                    System.out.println(ANSI_GREEN + " - O teste passou!" + ANSI_RESET);
                } else {
                    System.out.println(ANSI_RED + " - O teste falhou!" + ANSI_RESET);
                }
            }
            return;
        } 
        else if (args.length < 2) { 
            System.out.println(
                ANSI_BLUE+
                "Digite um dos comandos abaixo:\n" +
                "\tRodar programa: java Lab6 <tamanho do array> <número de threads>\n"+
                "\tRodar testes: java Lab6 --test"+
                ANSI_RESET
            );
            return;
        }

        N = Integer.parseInt(args[0]);
        nThreads = Integer.parseInt(args[1]);

        if (nThreads > N) { nThreads = N; }

        threads = new Thread[nThreads];
        array = new int[N];

        handle(threads, array, nThreads);

        System.out.print("Array = { ");
        for (int i : array) { 
            System.out.print(i+" ");
        }
        System.out.println("}");
    }
}