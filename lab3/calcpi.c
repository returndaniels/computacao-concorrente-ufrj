#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include "timer.h"

int nThreads;
long long int nTerms;

typedef struct {
    long long int id;
} tArg;


double getTerm(long long int n) {
    long long denominator = (2*n)+1;
    return pow(-1, n)*(1.0/denominator);
}

void *sumPISeries(void *arg) {
    double *sum; 
    tArg *args = (tArg *)arg;
    
   sum = (double*) calloc(1, sizeof(double));
   if(sum==NULL) {exit(1);}

    for(long long int i=args->id; i <= nTerms; i+=nThreads){
        *sum += getTerm(i);
    }
	pthread_exit((void *) sum);
}

int main(int argc, char **argv) {
	double start, end, delta;
    double PI, sum=0, *partialSum=0;
	pthread_t *tid;
    tArg *threads;

    if(argc < 3) {
        printf("Use o comando:\n \t%s <numero de elementos> <numero de threads>\n", argv[0]);
        return 1;
    }

    nTerms = atoll(argv[1]);
    nThreads = atoi(argv[2]);

    tid = (pthread_t *)malloc(sizeof(pthread_t) * nThreads);
    threads = (tArg *)malloc(sizeof(tArg) * nThreads);
    if (tid == NULL || threads == NULL){
		puts("ERRO--malloc");
		return 2;
	}

    GET_TIME(start);
    for(int i=0; i<nThreads; i++){
        (threads+i)->id = (long long int) i;
        if(pthread_create(tid + i, NULL, sumPISeries, (void *)(threads + i))){
         fprintf(stderr, "ERRO--pthread_create\n");
         return 3;
      }
    }

    for(int i=0; i<nThreads; i++) {
        if(pthread_join(*(tid+i), (void**) &partialSum)){
            fprintf(stderr, "ERRO--pthread_create\n");
            return 3;
        }
        sum += *partialSum;
    }
    PI = 4*sum;
	GET_TIME(end)
	delta = end - start;

    printf("Número de termos: %lli, Número de threads: %i, Tempode execução: %lf seg\n", nTerms, nThreads, delta);
    printf("o valor de pi calculado é:\t %.15lf\n", PI);
    printf("o valor de pi da lib math é:\t %.15lf\n", M_PI);
    printf("A diferença entre ambos é:\t %.15lf\n", M_PI-PI);

    return 0;
}