# Série para cálcular π
Programa concorrente para o problema de calcular a soma de uma serie de valores reais que aproxima o valor de π;

* O ganho do programa para quando recebe a entrada de número de threads ```2n``` é ```1,9664``` quando comparada a uma entrada ```n``` (ex: n=1).
* O resultado diverge para duas entradas com o mesmo número de termos que utilizem diferentes números de threads, devido a sequancia de termino das operções.
* Quanto maior o número de termos, maior é a precisão do resultado. 