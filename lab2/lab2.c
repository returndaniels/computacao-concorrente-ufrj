/**
/* Este programa concorrente, recebe um número de threades que usará para
/* multiplicar uma matriz por um vetor.
**/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include "timer.h"

float *mat1;
float *mat2;
float *output;
int nthreads;

typedef struct
{
	int id;
	int dim;
} argType;

void *operator(void *arg)
{
	argType *args = (argType *)arg;

	for (int row = args->id; row < args->dim; row += nthreads)
	{
		for (int col = 0; col < args->dim; col++)
		{
			for (int i = 0; i < args->dim; i++)
			{
				output[row * (args->dim) + col] += mat1[row * (args->dim) + i] * mat2[i * (args->dim) + col];
			}
		}
	}
	pthread_exit(NULL);
}

void help(char *arg)
{
	printf("Digite: %s <dimensao da matriz> <numero de threads>\n", arg);
	printf("Argumentos opcionais:\n");
	printf("\t--show-output\t\t Exibe a saída do programa\n");
	printf("\t--help\t\t\t Exibe as oções do programa\n");
}

int main(int argc, char **argv)
{
	int dim;
	pthread_t *tid;
	argType *args;
	double start, end, delta;

	char *option1 = "--help";
	char *option2 = "--show-output";

	if (argc == 2 && !strcmp(argv[1], option1))
	{
		help(argv[0]);
		return 1;
	}

	if (argc < 3)
	{
		help(argv[0]);
		return 1;
	}

	/** 
	 * ------------ ALOCAÇÃO -------------
	*/
	GET_TIME(start)
	dim = atoi(argv[1]);
	nthreads = atoi(argv[2]);
	if (nthreads > dim)
	{
		nthreads = dim;
	}

	if (dim == 0 || nthreads == 0)
	{
		help(argv[0]);
		return 1;
	}

	mat1 = (float *)malloc(sizeof(float) * dim * dim);
	if (mat1 == NULL)
	{
		printf("ERRO--malloc\n");
		return 2;
	}
	mat2 = (float *)malloc(sizeof(float) * dim * dim);
	if (mat2 == NULL)
	{
		printf("ERRO--malloc\n");
		return 2;
	}
	output = (float *)malloc(sizeof(float) * dim * dim);
	if (output == NULL)
	{
		printf("ERRO--malloc\n");
		return 2;
	}

	/** 
	 * ------------ INICIALIZAÇÃO -------------
	*/
	for (int i = 0; i < dim; i++)
	{
		for (int j = 0; j < dim; j++)
		{
			mat1[i * dim + j] = 1;
			mat2[i * dim + j] = 1;
		}
		output[i] = 0;
	}
	GET_TIME(end)
	delta = end - start;
	printf("Tempo de alocação e inicialização das estruturas: %lf\n", delta);

	/** 
	 * ------------ OPERAÇÃO -------------
	*/
	GET_TIME(start)
	tid = (pthread_t *)malloc(sizeof(pthread_t) * nthreads);
	if (tid == NULL)
	{
		puts("ERRO--malloc");
		return 2;
	}
	args = (argType *)malloc(sizeof(argType) * nthreads);
	if (args == NULL)
	{
		puts("ERRO--malloc");
		return 2;
	}

	for (int i = 0; i < nthreads; i++)
	{
		(args + i)->id = i;
		(args + i)->dim = dim;
		if (pthread_create(tid + i, NULL, operator, (void *)(args + i)))
		{
			puts("ERRO--pthread_create");
			return 3;
		}
	}

	for (int i = 0; i < nthreads; i++)
	{
		pthread_join(*(tid + i), NULL);
	}
	GET_TIME(end)
	delta = end - start;
	printf("Tempo da operação de multiplicação: %lf\n", delta);

	/** 
	* ------------ EXIBIÇÃO -------------
	*/
	GET_TIME(start)
	for (int i = 1; i < argc; i++)
	{
		int diff = strcmp(argv[i], option2);
		if (!diff)
		{
			puts("OUTPUT:");
			for (int i = 0; i < dim; i++)
			{
				printf("[ ");
				for (int j = 0; j < dim; j++)
				{
					printf("%.1f ", output[i * dim + j]);
				}
				puts("]");
			}
			break;
		}
	}
	GET_TIME(end)
	delta = end - start;
	printf("Tempo de exibição do resultado: %lf\n", delta);

	/** 
	* ------------ LIMPEZA -------------
	*/
	GET_TIME(start)
	free(mat1);
	free(mat2);
	free(output);
	free(args);
	free(tid);
	GET_TIME(end)
	delta = end - start;
	printf("Tempo para liberar o espaço alocado na memória: %lf\n", delta);

	return 0;
}
